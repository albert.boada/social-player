'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _SocialPlayer2 = require('./SocialPlayer.js');

var _SocialPlayer3 = _interopRequireDefault(_SocialPlayer2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var SoundcloudPlayer = function (_SocialPlayer) {
    _inherits(SoundcloudPlayer, _SocialPlayer);

    function SoundcloudPlayer() {
        _classCallCheck(this, SoundcloudPlayer);

        return _possibleConstructorReturn(this, (SoundcloudPlayer.__proto__ || Object.getPrototypeOf(SoundcloudPlayer)).apply(this, arguments));
    }

    _createClass(SoundcloudPlayer, [{
        key: 'doLoadAndPlay',
        value: function doLoadAndPlay(track) {
            var self = this;

            if (self.getPlayer()) {
                self.getPlayer().load(track.url, { auto_play: true, visual: true, callback: function callback() {
                        // this callback is called even when there is an error! (AFTER it...WTF!!)
                        if (!self.errored) {
                            self.onTrackLoaded();
                        }
                        self.errored = false;
                    } });
            } else {
                SC.oEmbed(track.url, { auto_play: true }).then(function (oEmbed) {
                    self.getWrapper().innerHTML = oEmbed.html.replace('<iframe', '<iframe allow="autoplay"').replace('"></iframe>', '&auto_play=true"></iframe>');
                    self.setPlayer(SC.Widget(self.getWrapper().querySelector('iframe')));

                    // Only called on player initial load, not when .load().
                    self.getPlayer().bind('ready', function () {
                        self.onTrackLoaded();
                    });

                    self.getPlayer().bind('finish', function () {
                        self.finished = true;
                        self.onTrackFinished();
                    });
                    self.getPlayer().bind('play', function () {
                        self.started = true;
                        self.paused = false;
                        self.onStateChange(_SocialPlayer3.default.PLAY);
                    });
                    self.getPlayer().bind('pause', function (info) {
                        if (info.relativePosition > 0.99) {
                            // Track is finished. "finish" event will be fired too.
                            return;
                        }
                        if (self.terminating) {
                            self.terminating = false;
                            return;
                        }
                        self.paused = true;
                        self.onStateChange(_SocialPlayer3.default.PAUSE);
                    });
                    self.getPlayer().bind('error', function (e) {
                        self.errored = true;
                        self.onTrackLoaded();
                        self.onTrackFinished();
                    });

                    self.seekAttention();

                    /*setTimeout(function () {
                        self.pause();
                        self.play();
                    }, 300);*/
                }).catch(function (e) {
                    var fakeSCPlayer = { pause: function pause() {} };
                    self.setPlayer(fakeSCPlayer);
                    self.onTrackLoaded();
                    self.onTrackFinished();
                    self.setPlayer(undefined);
                }).catch(function (e) {
                    console.log(e);
                });
            }
        }
    }, {
        key: 'doPlay',
        value: function doPlay() {
            this.getPlayer().play();
        }
    }, {
        key: 'doPause',
        value: function doPause() {
            this.getPlayer().pause();
        }
    }, {
        key: 'doStop',
        value: function doStop() {
            this.doPause();
        }
    }]);

    return SoundcloudPlayer;
}(_SocialPlayer3.default);

exports.default = SoundcloudPlayer;
//# sourceMappingURL=SoundcloudPlayer.js.map