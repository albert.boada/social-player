"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var TracksCollectionItem = function () {
    function TracksCollectionItem(source, url, moreInfo) {
        var original = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;

        _classCallCheck(this, TracksCollectionItem);

        this.uid = new Date().valueOf() + Math.random().toFixed(16).substring(2);
        this.source = source;
        this.url = url;
        this.moreInfo = moreInfo;
        this.original = original;
    }

    _createClass(TracksCollectionItem, null, [{
        key: "makeFromOriginal",
        value: function makeFromOriginal(original) {
            return new TracksCollectionItem(original.source, original.url, original.moreInfo, original);
        }
    }]);

    return TracksCollectionItem;
}();

exports.default = TracksCollectionItem;
//# sourceMappingURL=TracksCollectionItem.js.map