'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _SocialPlayer2 = require('./SocialPlayer.js');

var _SocialPlayer3 = _interopRequireDefault(_SocialPlayer2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var MixcloudPlayer = function (_SocialPlayer) {
    _inherits(MixcloudPlayer, _SocialPlayer);

    function MixcloudPlayer() {
        _classCallCheck(this, MixcloudPlayer);

        return _possibleConstructorReturn(this, (MixcloudPlayer.__proto__ || Object.getPrototypeOf(MixcloudPlayer)).apply(this, arguments));
    }

    _createClass(MixcloudPlayer, [{
        key: 'doLoadAndPlay',
        value: function doLoadAndPlay(track) {
            var self = this;

            if (self.getPlayer()) {
                self.getPlayer().load(track.url, true).then(function () {
                    self.onTrackLoaded();
                });
            } else {
                self.getWrapper().innerHTML = '\n                <iframe\n                    id="mcplayer-iframe"\n                    width="100%"\n                    height="120"\n                    src="https://www.mixcloud.com/widget/iframe/?light=1&autoplay=1&feed=' + encodeURIComponent(track.url) + '"\n                    frameborder="0"\n                    allow="autoplay"\n                ></iframe>\n            ';

                var player = Mixcloud.PlayerWidget(document.getElementById('mcplayer-iframe'));

                player.ready.then(function () {
                    self.setPlayer(player);

                    self.onTrackLoaded();

                    self.getPlayer().events.ended.on(function () {
                        self.finished = true;
                        self.onTrackFinished();
                    });

                    self.getPlayer().events.pause.on(function () {
                        self.getPlayer().getPosition().then(function (position) {
                            self.getPlayer().getDuration().then(function (duration) {
                                if (self.terminating) {
                                    self.terminating = false;
                                    return;
                                }
                                if (position === duration) {
                                    // Track is finished. "ended" event will be fired too.
                                    return;
                                }
                                self.paused = true;
                                self.onStateChange(_SocialPlayer3.default.PAUSE);
                            });
                        });
                    });

                    self.getPlayer().events.play.on(function () {
                        self.started = true;
                        self.paused = false;
                        self.onStateChange(_SocialPlayer3.default.PLAY);
                    });
                });

                self.seekAttention();
            }
        }
    }, {
        key: 'doPlay',
        value: function doPlay() {
            this.getPlayer().play();
        }
    }, {
        key: 'doPause',
        value: function doPause() {
            this.getPlayer().pause();
        }
    }, {
        key: 'doStop',
        value: function doStop() {
            this.doPause();
        }
    }]);

    return MixcloudPlayer;
}(_SocialPlayer3.default);

exports.default = MixcloudPlayer;
//# sourceMappingURL=MixcloudPlayer.js.map