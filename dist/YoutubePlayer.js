'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _SocialPlayer2 = require('./SocialPlayer.js');

var _SocialPlayer3 = _interopRequireDefault(_SocialPlayer2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var YoutubePlayer = function (_SocialPlayer) {
    _inherits(YoutubePlayer, _SocialPlayer);

    function YoutubePlayer() {
        _classCallCheck(this, YoutubePlayer);

        return _possibleConstructorReturn(this, (YoutubePlayer.__proto__ || Object.getPrototypeOf(YoutubePlayer)).apply(this, arguments));
    }

    _createClass(YoutubePlayer, [{
        key: 'doLoadAndPlay',
        value: function doLoadAndPlay(track) {
            var self = this;

            if (self.getPlayer()) {
                if (!self.isMobile()) {
                    self.getPlayer().loadVideoById(track.url);
                } else {
                    self.getPlayer().cueVideoById(track.url);
                }
                self.onTrackLoaded();
            } else {
                var containerId = 'youtubeembed' + Math.random().toString(16).slice(2);
                self.getWrapper().innerHTML = '<div id="' + containerId + '"></div>';
                var player = new YT.Player(containerId, {
                    width: '100%',
                    height: '100%',
                    videoId: track.url,
                    events: {
                        onReady: function onReady(event) {
                            self.setPlayer(player);
                            self.seekAttention();
                            if (!self.isMobile()) {
                                self.play();
                            }
                            self.onTrackLoaded();
                            //event.target.playVideo();
                        },
                        onStateChange: function onStateChange(event) {
                            // TODO https://stackoverflow.com/questions/41239944/youtube-api-event-distinguish-pause-from-seek-buffer
                            if (event.data === YT.PlayerState.ENDED) {
                                self.finished = true;
                                self.onTrackFinished();
                            } else if (event.data === YT.PlayerState.PAUSED) {
                                if (self.terminating) {
                                    self.terminating = false;
                                    return;
                                }
                                self.paused = true;
                                self.onStateChange(_SocialPlayer3.default.PAUSE);
                            } else if (event.data === YT.PlayerState.PLAYING) {
                                self.started = true;
                                self.paused = false;
                                self.onStateChange(_SocialPlayer3.default.PLAY);
                            }
                        },
                        onError: function onError() {
                            self.onTrackFinished();
                        }
                    }
                });
            }
        }
    }, {
        key: 'doPlay',
        value: function doPlay() {
            if (this.isMobile()) {
                return;
            }

            this.getPlayer().playVideo();
        }
    }, {
        key: 'doPause',
        value: function doPause() {
            if (this.isMobile()) {
                return;
            }

            this.getPlayer().pauseVideo();
        }
    }, {
        key: 'doStop',
        value: function doStop() {
            if (this.isMobile()) {
                return;
            }

            this.getPlayer().stopVideo();
        }
    }]);

    return YoutubePlayer;
}(_SocialPlayer3.default);

exports.default = YoutubePlayer;
//# sourceMappingURL=YoutubePlayer.js.map