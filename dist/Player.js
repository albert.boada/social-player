'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.PUSH_MODE_PREPEND = exports.PUSH_MODE_APPEND = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _TracksCollection = require('./TracksCollection.js');

var _TracksCollection2 = _interopRequireDefault(_TracksCollection);

var _TracksCollectionItem = require('./TracksCollectionItem.js');

var _TracksCollectionItem2 = _interopRequireDefault(_TracksCollectionItem);

var _PlayerTransition = require('./PlayerTransition.js');

var _PlayerTransition2 = _interopRequireDefault(_PlayerTransition);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var PUSH_MODE_APPEND = exports.PUSH_MODE_APPEND = 1;
var PUSH_MODE_PREPEND = exports.PUSH_MODE_PREPEND = 2;

var Player = function () {
    /**
     * @param SocialPlayer[] players
     * @param function onUpdate
     * @param function onUpdateStart
     */
    function Player(players, onUpdate, onUpdateStart, onStateChange) {
        _classCallCheck(this, Player);

        /**
         * @var SocialPlayer[] players
         */
        this.players = players;

        this.onUpdate = onUpdate;
        this.onUpdateStart = onUpdateStart;
        this.onStateChange = onStateChange;

        /**
         * @var TracksCollection tracks
         *
         * Collection which stores the tracks available for the Player to play :)
         */
        this.tracks = new _TracksCollection2.default();

        /**
         * @var TracksCollection playedTracksInPlayback
         *
         * Collection used for registering all the played tracks during the
         * current Playback.
         *
         * What's exactly a Playback?
         * A Playback is one continous session of played tracks, without manual
         * alteration of its flow. A Playback can start either when the Player goes
         * from stopped to playing, or when, while playing, a track is manually
         * imposed; and lasts for as long as the Player is not stopped (e.g. the
         * Player can be stopped manually; the Player can be stopped when reaching
         * the bottom of the Playlist and looping is off; etc.), or a track is
         * manually imposed.
         *
         * This collection could potentially have repeated tracks (e.g. the same
         * list of tracks can loop many times, resulting in the same tracks being
         * added multiple times), causing unexpected results when querying it.
         * This is why this collection is not meant to directly store the original
         * TrackCollectionItems that `this.tracks` contains, but new and UNIQUE
         * TrackCollectionItems which will reference the original TrackCollectionItem
         * (i.e. `item.original`), instead.
         */
        this.playedTracksInPlayback;

        /**
         * @var TracksCollection playedTracksInCycle
         *
         * Collection used for registering all the played tracks during the
         * current Cycle, UNIQUELY.
         *
         * What's exactly a Cycle?
         * A Cycle is a time period in which not all tracks in `this.tracks` have
         * been played at least once. When all tracks in `this.tracks` have been
         * played, a new Cycle begins.
         *
         * When registering the already played tracks in the current Cycle, it makes
         * no sense to store them more than once. Being so, this collection just
         * DOES store original TrackCollectionItems, unlike `this.playedTracksInPlayback`.
         */
        this.playedTracksInCycle;

        this.repeat = false;
        this.repeatTrack = false;
        this.shuffle = false;

        this.pushMode = PUSH_MODE_APPEND;

        /**
         * @var SocialPlayer currentPlayer
         */
        this.currentPlayer;

        /**
         * @var TrackCollectionItem currentPlaybackTrack
         *
         * Holds the "wrapped" TrackCollectionItem being currently played (i.e. from
         * the `this.playedTracksInPlayback` collection).
         */
        this.currentPlaybackTrack;

        /**
         * @var number loopsCount
         */
        this.loopsCount;

        this._resetPlayback();
    }

    /**
     * @param TracksCollection tracksCollection
     */


    _createClass(Player, [{
        key: 'loadTracks',
        value: function loadTracks(tracksCollection) {
            this._resetPlayback();
            this.tracks = tracksCollection;
        }
    }, {
        key: 'getCurrentTrack',
        value: function getCurrentTrack() {
            return this.currentPlaybackTrack ? this.currentPlaybackTrack.original : undefined;
        }

        /**
         * @param TrackCollectionItem track
         */

    }, {
        key: 'addTrack',
        value: function addTrack(track) {
            if (this.pushMode === PUSH_MODE_PREPEND) {
                this.tracks.pushFirst(track);
            } else {
                this.tracks.push(track);
            }
        }
    }, {
        key: 'removeTrackByUid',
        value: function removeTrackByUid(uid) {
            var isCurrentTrack = this._hasPlaybackStarted() && this.getCurrentTrack().uid === uid;
            if (isCurrentTrack) {
                this._doRemoveCurrentTrack();
            } else {
                this._doRemoveTrackByUid(uid);
            }
        }
    }, {
        key: 'startPlaying',
        value: function startPlaying() {
            var transition = this._getFirstTransition();
            this._applyTransition(transition);
        }
    }, {
        key: 'playNext',
        value: function playNext() {
            if (!this._hasPlaybackStarted()) {
                throw 'PLAYER_CANT_SKIP_NON_STARTED_PLAYBACK';
            }

            var transition = this._getNextTransition();
            this._applyTransition(transition);
        }
    }, {
        key: 'play',
        value: function play() {
            if (!this._hasPlaybackStarted()) {
                this.startPlaying();
                return;
            }

            this.resume();
        }
    }, {
        key: 'pause',
        value: function pause() {
            if (!this._hasPlaybackStarted()) {
                throw 'PLAYER_CANT_PAUSE_NON_STARTED_PLAYBACK';
            }

            this.currentPlayer.pause();
        }
    }, {
        key: 'resume',
        value: function resume() {
            if (!this._hasPlaybackStarted()) {
                throw 'PLAYER_CANT_RESUME_NON_STARTED_PLAYBACK';
            }

            this.currentPlayer.play();
        }
    }, {
        key: 'skip',
        value: function skip() {
            this.playNext();
        }
    }, {
        key: 'stop',
        value: function stop() {
            var transition = new _PlayerTransition2.default();

            this._applyTransition(transition);
        }
    }, {
        key: 'playPrev',
        value: function playPrev() {
            if (!this._hasPlaybackStarted()) {
                throw 'PLAYER_CANT_BACKSKIP_NON_STARTED_PLAYBACK';
            }

            var transition = this._getPrevTransition();
            this._applyTransition(transition);
        }
    }, {
        key: 'playTrackByUid',
        value: function playTrackByUid(uid) {
            var transition = new _PlayerTransition2.default();
            transition.track = this.tracks.getTrackByUid(uid);
            transition.willResetPlayback = true;

            this._applyTransition(transition);
        }
    }, {
        key: 'playTrackByNumber',
        value: function playTrackByNumber(number) {
            var uid = this.tracks.getTrackByNumber(number).uid;
            this.playTrackByUid(uid);
        }
    }, {
        key: 'toggleRepeat',
        value: function toggleRepeat() {
            this.repeat = !this.repeat;
            // this._resetRepeat()? <- but don't reset "this.loopsCount", ok???
            return this.repeat;
        }
    }, {
        key: 'toggleRepeatTrack',
        value: function toggleRepeatTrack() {
            this.repeatTrack = !this.repeatTrack;
            return this.repeatTrack;
        }
    }, {
        key: 'toggleShuffle',
        value: function toggleShuffle() {
            this.shuffle = !this.shuffle;
            this._resetLoopsCount();
            this._resetPlayedTracks();
            return this.shuffle;
        }
    }, {
        key: '_resetPlayback',
        value: function _resetPlayback() {
            this.currentPlaybackTrack = undefined;
            this.currentPlayer = undefined;
            this._resetLoopsCount();
            this._resetPlayedTracks();
        }
    }, {
        key: '_resetLoopsCount',
        value: function _resetLoopsCount() {
            this.loopsCount = 0;
        }
    }, {
        key: '_resetPlayedTracks',
        value: function _resetPlayedTracks() {
            this.playedTracksInCycle = new _TracksCollection2.default();
            this.playedTracksInPlayback = new _TracksCollection2.default();
        }
    }, {
        key: '_hasPlaybackStarted',
        value: function _hasPlaybackStarted() {
            return typeof this.currentPlaybackTrack !== 'undefined';
        }
    }, {
        key: '_doRemoveTrackByUid',
        value: function _doRemoveTrackByUid(uid) {
            this.tracks.removeTrackByUid(uid);
            this.playedTracksInPlayback.removeTracksByOriginalUid(uid);
            this.playedTracksInCycle.removeTrackByUid(uid);
        }
    }, {
        key: '_doRemoveCurrentTrack',
        value: function _doRemoveCurrentTrack() {
            var currentTrack = this.getCurrentTrack();

            var nextTransition = this._getNextTransition();
            if (nextTransition.track && nextTransition.track.uid === currentTrack.uid) {
                nextTransition.track = undefined;
            }

            this._doRemoveTrackByUid(currentTrack.uid);
            this._applyTransition(nextTransition);
        }
    }, {
        key: '_getFirstTransition',
        value: function _getFirstTransition() {
            var transition = void 0;
            if (this.shuffle) {
                transition = this._getShuffleTransition();
            } else {
                transition = new _PlayerTransition2.default();
                transition.track = this.tracks.getFirstTrack();
            }
            transition.willResetPlayback = true;

            return transition;
        }
    }, {
        key: '_getNextTransition',
        value: function _getNextTransition() {
            if (this.repeatTrack) {
                return this._getSameTrackTransition();
            }

            if (this.shuffle) {
                return this._getShuffleTransition();
            }

            var transition = new _PlayerTransition2.default();
            try {
                transition.track = this.tracks.getTracksNextTrack(this.getCurrentTrack());
            } catch (e) {
                if (this.repeat) {
                    transition.track = this.tracks.getFirstTrack();
                    transition.willLoop = true;
                    transition.willResetCycle = true;
                }
            }

            return transition;
        }
    }, {
        key: '_getPrevTransition',
        value: function _getPrevTransition() {
            if (this.repeatTrack) {
                return this._getSameTrackTransition();
            }

            if (this.shuffle) {
                return this._getPrevShuffleTransition();
            }

            var transition = new _PlayerTransition2.default();
            try {
                transition.track = this.tracks.getTracksPrevTrack(this.getCurrentTrack());
            } catch (e) {
                if (this.loopsCount > 0) {
                    transition.track = this.tracks.getLastTrack();
                    transition.willUnloop = true;
                }
            }
            return transition;
        }
    }, {
        key: '_getSameTrackTransition',
        value: function _getSameTrackTransition() {
            var transition = new _PlayerTransition2.default();
            transition.track = this.currentPlaybackTrack;
            transition.isPlaybackTrack = true;
            return transition;
        }
    }, {
        key: '_getShuffleTransition',
        value: function _getShuffleTransition() {
            var transition = new _PlayerTransition2.default();
            try {
                transition.track = this.tracks.getRandomTrack(this.playedTracksInCycle);
            } catch (e) {
                if ('BLACKLISTED_ALL_TRACKS' === e && this.repeat) {
                    transition.track = this.tracks.getRandomTrack(new _TracksCollection2.default());
                    transition.willResetCycle = true;
                    transition.willLoop = true;
                }
            }
            return transition;
        }
    }, {
        key: '_getPrevShuffleTransition',
        value: function _getPrevShuffleTransition() {
            var transition = new _PlayerTransition2.default();
            try {
                transition.track = this.playedTracksInPlayback.getTracksPrevTrack(this.currentPlaybackTrack);
                transition.isPlaybackTrack = true;
                transition.willTrack = false;
            } catch (e) {}
            return transition;
        }
    }, {
        key: '_applyTransition',
        value: function _applyTransition(transition) {
            //console.log('Transition: ', transition);
            if (!transition) {
                throw 'NO_TRANSITION';
            }

            var originalTrack = transition.isPlaybackTrack ? transition.track.original : transition.track;
            this._notifyTransitionStarted(originalTrack);

            if (this.currentPlayer) {
                this.currentPlayer.stop();
            }

            if (transition.isStop()) {
                this._resetPlayback();
                this._notifyTransitionFinished();
                return;
            } else {
                var nextPlayer = this._getPlayerForTrack(transition.track);

                nextPlayer.loadAndPlay(transition.track, function () {
                    if (transition.willResetPlayback) {
                        this._resetPlayback();
                    }

                    if (transition.willLoop) {
                        this.loopsCount++;
                    } else if (transition.willUnloop) {
                        this.loopsCount--;
                    }

                    if (transition.willResetCycle) {
                        this.playedTracksInCycle = new _TracksCollection2.default();
                    }

                    this.currentPlayer = nextPlayer;

                    var playbackTrack = void 0;
                    if (transition.isPlaybackTrack) {
                        playbackTrack = transition.track;
                    } else {
                        playbackTrack = _TracksCollectionItem2.default.makeFromOriginal(transition.track);
                    }

                    this.currentPlaybackTrack = playbackTrack;

                    if (transition.willTrack) {
                        this.playedTracksInPlayback.push(this.currentPlaybackTrack);
                        this.playedTracksInCycle.pushUnique(this.currentPlaybackTrack.original);
                    }

                    this._notifyTransitionFinished();
                }.bind(this), this._onTrackFinished.bind(this), this.onStateChange);
            }
        }
    }, {
        key: '_getPlayerForTrack',
        value: function _getPlayerForTrack(track) {
            if (track) {
                return this._getPlayerForSource(track.source);
            }

            return undefined;
        }
    }, {
        key: '_getPlayerForSource',
        value: function _getPlayerForSource(source) {
            if (typeof this.players[source] === 'undefined') {
                throw 'NO_PLAYER_FOR(' + source + ')';
            }

            return this.players[source];
        }
    }, {
        key: '_onTrackFinished',
        value: function _onTrackFinished() {
            this.playNext();
        }
    }, {
        key: '_notifyTransitionStarted',
        value: function _notifyTransitionStarted(futureTrack) {
            if (this.onUpdateStart) {
                this.onUpdateStart(futureTrack);
            }
        }
    }, {
        key: '_notifyTransitionFinished',
        value: function _notifyTransitionFinished() {
            if (this.onUpdate) {
                this.onUpdate(this.getCurrentTrack());
            }
        }
    }]);

    return Player;
}();

exports.default = Player;
//# sourceMappingURL=Player.js.map