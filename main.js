import Player from './src/Player.js';
import TracksCollection from './src/TracksCollection.js';
import TracksCollectionItem from './src/TracksCollectionItem.js';
import $ from './node_modules/jquery/dist/jquery.min.js';
import YoutubePlayer from './src/YoutubePlayer.js';
import SoundcloudPlayer from './src/SoundcloudPlayer.js';
import MixcloudPlayer from './src/MixcloudPlayer.js';
import NoopPlayer from './src/NoopPlayer.js';

function renderPlaylist(tracks) {
    for (let i = 0; i < tracks.length; i++) {
        let track = tracks[i];
        $("#tracklist").append(`
            <div data-track-id="${track.uid}">
                <button class="btn-play">></button> ${i+1}. ${track.moreInfo.track.title} (id:${track.uid})
            </div>
        `);
    }
}

function highlightPlaylistTrack(id, color) {
    $("#tracklist > *").css({"backgroundColor": "transparent"});
    if (id !== 'undefined') {
        $(`[data-track-id=${id}]`).css({"backgroundColor": color});
    }
}

/**
 * @param array playlist
 */
function buildTracksCollectionFromCustomPlaylist(playlist) {
    const tracksCollection = new TracksCollection();
    for (let i = 0; i < playlist.length; i++) {
        let playlistTrack = playlist[i];
        tracksCollection.push(
            new TracksCollectionItem(
                playlistTrack.track.source,
                playlistTrack.track.url,
                playlistTrack
            )
        );
    }
    return tracksCollection;
}

const playlist = [
    { id: 56, track: { title: 'Anjunadeep08', url: 'DcGcTLJ2WHs', source: 'youtube' } },
    { id: 23, track: { title: 'Anima', url: 'https://soundcloud.com/colin-e-fisher/anima', source: 'soundcloud' } },
    { id: 32, track: { title: 'Enigma', url: 'https://soundcloud.com/colin-e-fisher/enigma', source: 'soundcloud' } },
    { id: 12, track: { title: 'Forever', url: 'https://soundcloud.com/nomad-soul-collective/forever-ft-lucinda', source: 'soundcloud' } },
    { id: 44, track: { title: 'Ohm', url: 'https://soundcloud.com/nomad-soul-collective/ohm', source: 'soundcloud' } },
    { id: 111, track: { title: 'Anjunadeep08', url: 'DcGcTLJ2WHs', source: 'youtube' } },
    { id: 555, track: { title: 'Bayer', url: 'incDMboT93o', source: 'youtube' } },
    { id: 444, track: { title: 'Blizzard', url: 'https://soundcloud.com/luminosityevents-1/the-blizzard-luminosity-beach-festival-2018', source: 'soundcloud' } },
    { id: 31, track: { title: 'Catalyst', url: 'oKHWJEEzt8o', source: 'youtube' } },
    { id: 32, track: { title: 'Secret of the forest', url: 'amrKGuuSZlg', source: 'youtube' } },
    { id: 33, track: { title: 'Aquatic ambiance', url: 'jbuQMNuSYEI', source: 'youtube' } },
    { id: 34, track: { title: 'Breath of the wild main theme guitar cover', url: 'bW1Mz7ngCA4', source: 'youtube' } },
    { id: 34, track: { title: 'Anjunadeep Mixcloud', url: '/anjunadeep/the-anjunadeep-edition-200-with-james-grant-jody-wisternoff-live-from-miami/', source: 'mixcloud' } },
    { id: 38, track: { title: 'Anjunadeep Mixcloud', url: '/anjunadeep/the-anjunadeep-edition-200-with-james-grant-jody-wisternoff-live-from-miami/', source: 'mixcloud' } },
];

function onPlayerUpdated(currentTrack) {
    highlightPlaylistTrack(currentTrack ? currentTrack.uid : undefined, 'skyblue');
}

function onPlayerUpdateStarted(futureTrack) {
    highlightPlaylistTrack(futureTrack ? futureTrack.uid : undefined, 'yellow');
}

const players = {
    'youtube'    : new YoutubePlayer(document.getElementById('ytplayer')),
    'soundcloud' : new SoundcloudPlayer(document.getElementById('scplayer')),
    'mixcloud'   : new MixcloudPlayer(document.getElementById('mcplayer')),
};

const player = new Player(players, onPlayerUpdated, onPlayerUpdateStarted, (state, affectedTrack) => { console.log('FIRED!', state, affectedTrack); });

const tracksCollection = buildTracksCollectionFromCustomPlaylist(playlist);

player.loadTracks(tracksCollection);

renderPlaylist(tracksCollection.getTracks());

/**************
 * DOM Events
 */

window.onYouTubeIframeAPIReady = function () {
    // player.play();
};

$('#play').on('click', function () {
    player.play();
});

$('#next').on('click', function () {
    player.skip();
});

$('#pause').on('click', function () {
    player.pause();
});

$('#prev').on('click', function () {
    player.playPrev();
});

$('#repeat').on('click', function () {
    const repeat = player.toggleRepeat();
    $(this).css('background-color', repeat ? 'yellow' : 'transparent');
});

$('#repeat-track').on('click', function () {
    const repeat = player.toggleRepeatTrack();
    $(this).css('background-color', repeat ? 'yellow' : 'transparent');
});

$('#shuffle').on('click', function () {
    const shuffle = player.toggleShuffle();
    $(this).css('background-color', shuffle ? 'yellow' : 'transparent');
});

$(document).on('click', '#tracklist .btn-play', function (e) {
    const $button = $(this);
    const $track = $button.closest('[data-track-id]');
    const trackUid = $track.attr('data-track-id');
    player.playTrackByUid(trackUid);
});
