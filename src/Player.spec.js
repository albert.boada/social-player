import chai from 'chai';
import path from 'path';
import Player, {PUSH_MODE_PREPEND} from './Player.js';
import TracksCollection from './TracksCollection.js';
import TracksCollectionItem from './TracksCollectionItem.js';
import NoopPlayer from './NoopPlayer.js';

const should = chai.should();

describe('Player', () => {
    let player;
    const pickedByUidErrorMsg = 'Track with uid {uid} does not exist.';
    const pickedByNumberErrorMsg = 'Track with number #{number} does not exist.';

    beforeEach(() => {
        const players = {
            'noop': new NoopPlayer('where')
        };
        player = new Player(players);
    });

    describe('manipulation', () => {
        describe('no tracks', () => {
            it('should load a tracks collection', () => {
                const tracksCollection = getDefaultTracksCollection();

                player.loadTracks(tracksCollection);
                player.tracks.should.equal(tracksCollection);
            });
        });

        describe('pre-loaded tracks', () => {
            beforeEach(() => {
                const tracksCollection = getDefaultTracksCollection();
                player.loadTracks(tracksCollection);
            });

            it('should remove a track when requested', () => {
                const trackToRemove = player.tracks.getTrackByNumber(2);

                player.removeTrackByUid(trackToRemove.uid);
                player.tracks.getTracks().should.not.contain(trackToRemove);
            });

            it('should not fail when removing a track that does not exist', () => {
                let thrown = false;

                try {
                    player.removeTrackByUid(99999);
                } catch (e) {
                    thrown = true;
                }
                thrown.should.be.false;
            });

            it('should add a new track when requested', () => {
                const newTrack = new TracksCollectionItem('noop', 'url99', {});

                player.addTrack(newTrack);
                player.tracks.getLastTrack().should.equal(newTrack);
            });

            it('should add a new track at the beginning when requested', () => {
                const newTrack = new TracksCollectionItem('noop', 'url99', {});

                player.pushMode = PUSH_MODE_PREPEND;

                player.addTrack(newTrack);
                player.tracks.getFirstTrack().should.equal(newTrack);
            });
        });
    });

    describe('playback', () => {
        beforeEach(() => {
            const tracksCollection = getDefaultTracksCollection();
            player.loadTracks(tracksCollection);
        });

        describe('normal', () => {
            it('should play the first track if nothing was playing and "play" is requested.', () => {
                should.equal(player.getCurrentTrack(), undefined);
                player.play();
                player.getCurrentTrack().url.should.equal('url1');
            });

            it('should play the first track if nothing was playing and "startPlaying" is requested.', () => {
                should.equal(player.getCurrentTrack(), undefined);
                player.startPlaying();
                player.getCurrentTrack().url.should.equal('url1');
            });

            it('should play the first track if something was playing and "startPlaying" is requested.', () => {
                const firstTrack = player.tracks.getFirstTrack();

                player.startPlaying();
                player.playNext();
                player.playNext();
                player.startPlaying();
                player.getCurrentTrack().should.equal(firstTrack);
            });

            it('should play the specific requested track by uid if it exists.', () => {
                const uid = player.tracks.getTrackByNumber(3).uid;

                player.playTrackByUid(uid);
                player.getCurrentTrack().url.should.equal('url3');
            });

            it('should not play the requested track by uid if it does not exist.', () => {
                let thrown;

                try {
                    player.playTrackByUid(999);
                } catch (e) {
                    thrown = e;
                }
                thrown.should.equal(pickedByUidErrorMsg.replace('{uid}', 999));
                should.equal(player.getCurrentTrack(), undefined);
            });

            it('should play the specific requested track by number if it exists.', () => {
                player.playTrackByNumber(3);
                player.getCurrentTrack().url.should.equal('url3');
            });

            it('should not play a requested track by number if it does not exist.', () => {
                let thrown;

                try {
                    player.playTrackByNumber(999);
                } catch (e) {
                    thrown = e;
                }
                thrown.should.equal(pickedByNumberErrorMsg.replace('{number}', 999));
                should.equal(player.getCurrentTrack(), undefined);
            });

            it('should stop playback when "stop" is requested.', () => {
                player.playTrackByNumber(3);
                player.stop();
                should.equal(player.getCurrentTrack(), undefined);
            });

            it('should not allow requesting "next" on a non-started playback.', () => {
                let thrown;

                try {
                    player.playNext();
                } catch (e) {
                    thrown = e;
                }
                thrown.should.equal('PLAYER_CANT_SKIP_NON_STARTED_PLAYBACK');
            });

            it('should not allow requesting "previous" on a non-started playback.', () => {
                let thrown;

                try {
                    player.playPrev();
                } catch (e) {
                    thrown = e;
                }
                thrown.should.equal('PLAYER_CANT_BACKSKIP_NON_STARTED_PLAYBACK');
            });

            it('should play the succeeding track when "next" is requested. It exists.', () => {
                player.startPlaying();
                player.playNext();
                player.getCurrentTrack().url.should.equal('url2');
                player.playNext();
                player.getCurrentTrack().url.should.equal('url3');
                player.playNext();
                player.getCurrentTrack().url.should.equal('url4');
            });

            it('should play the preceding track when "previous" is requested. It exists.', () => {
                player.playTrackByNumber(4);
                player.playPrev();
                player.getCurrentTrack().url.should.equal('url3');
                player.playPrev();
                player.getCurrentTrack().url.should.equal('url2');
                player.playPrev();
                player.getCurrentTrack().url.should.equal('url1');
            });

            it('should not play any track when "next" is requested and current track is the last one.', () => {
                const lastTrack = player.tracks.getLastTrack();

                player.playTrackByUid(lastTrack.uid);
                player.playNext();
                should.equal(player.getCurrentTrack(), undefined);
            });

            it('should not play any track when "previous" is requested and current track is the first one.', () => {
                player.startPlaying();
                player.playPrev();
                should.equal(player.getCurrentTrack(), undefined);
            });

            it('should be able to do a whole "forth-and-back" run consistently.', () => {
                player.startPlaying();
                player.playNext();
                player.playNext();
                player.playNext();
                player.getCurrentTrack().url.should.equal('url4');

                player.playPrev();
                player.playPrev();
                player.playPrev();
                player.getCurrentTrack().url.should.equal('url1');
            });

            it('should play dynamically added tracks at the end of the playback.', () => {
                const newTrack1 = new TracksCollectionItem('noop', 'url98', {});
                const newTrack2 = new TracksCollectionItem('noop', 'url99', {});

                player.playTrackByUid(player.tracks.getLastTrack().uid);

                player.addTrack(newTrack1);
                player.addTrack(newTrack2);

                player.playNext();
                player.getCurrentTrack().should.equal(newTrack1);

                player.playNext();
                player.getCurrentTrack().should.equal(newTrack2);

                player.playNext();
                should.equal(player.getCurrentTrack(), undefined);
            });

            it('should play dynamically prepended tracks at the beginning of the playback.', () => {
                const newTrack1 = new TracksCollectionItem('noop', 'url98', {});
                const newTrack2 = new TracksCollectionItem('noop', 'url99', {});

                player.pushMode = PUSH_MODE_PREPEND;

                player.addTrack(newTrack1);
                player.addTrack(newTrack2);

                player.startPlaying();
                player.getCurrentTrack().should.equal(newTrack2);

                player.playNext();
                player.getCurrentTrack().should.equal(newTrack1);

                player.playNext();
                player.getCurrentTrack().url.should.equal('url1');
            });

            it('should not play a just-removed track when the track was next and "next" is requested.', () => {
                player.startPlaying();
                player.playNext();
                player.getCurrentTrack().url.should.equal('url2');

                const nextTrack = player.tracks.getTracksNextTrack(player.getCurrentTrack());
                player.removeTrackByUid(nextTrack.uid);

                player.playNext();
                player.getCurrentTrack().should.not.equal(nextTrack);
                player.getCurrentTrack().url.should.equal('url4');
            });

            it('should not play a just-removed track when the track was previous and "previous" is requested.', () => {
                player.playTrackByNumber(4);
                player.playPrev();
                player.getCurrentTrack().url.should.equal('url3');

                const prevTrack = player.tracks.getTracksPrevTrack(player.getCurrentTrack());
                player.removeTrackByUid(prevTrack.uid);

                player.playPrev();
                player.getCurrentTrack().should.not.equal(prevTrack);
                player.getCurrentTrack().url.should.equal('url1');
            });

            it('should automatically jump to the next track when the current one gets removed,', () => {
                player.startPlaying();
                player.removeTrackByUid(player.getCurrentTrack().uid);
                player.getCurrentTrack().url.should.equal('url2');
            });

            it('should stop when the current track gets removed and was the last one,', () => {
                const lastTrack = player.tracks.getLastTrack();

                player.playTrackByUid(lastTrack.uid);
                player.removeTrackByUid(player.getCurrentTrack().uid);
                should.equal(player.getCurrentTrack(), undefined);
            });
        });

        describe('with repeat on', () => {
            beforeEach(() => {
                player.repeat = true;
            });

            it('should play the first track again after the last track.', () => {
                const firstTrack = player.tracks.getFirstTrack();
                const lastTrack = player.tracks.getLastTrack();
                player.playTrackByUid(lastTrack.uid);
                player.playNext();
                player.getCurrentTrack().should.equal(firstTrack);
            });

            it('should play the last track when "previous" is requested on the first track, as long as this loop has happened before in the opposite direction.', () => {
                const firstTrack = player.tracks.getFirstTrack();
                const lastTrack = player.tracks.getLastTrack();

                player.startPlaying();
                skip(player.tracks.getNumberOfTracks() * 2, player); // 2 full loops
                player.getCurrentTrack().should.equal(firstTrack);

                player.playPrev();
                player.getCurrentTrack().should.equal(lastTrack);

                skipBack(player.tracks.getNumberOfTracks(), player);
                player.getCurrentTrack().should.equal(lastTrack); // 1 full loop back

                skipBack(player.tracks.getNumberOfTracks() - 1, player); // from last track to first track, backwards
                player.getCurrentTrack().should.equal(firstTrack);

                player.playPrev();
                should.equal(player.getCurrentTrack(), undefined);
            });

            it('should play the last track when "previous" is requested on the first track, as long as this loop has happened before in the opposite direction, even if "repeat" is currently off.', () => {
                const firstTrack = player.tracks.getFirstTrack();
                const lastTrack = player.tracks.getLastTrack();

                player.startPlaying();
                skip(player.tracks.getNumberOfTracks() * 2, player); // 2 full loops
                player.getCurrentTrack().should.equal(firstTrack);

                player.playPrev();
                player.getCurrentTrack().should.equal(lastTrack);

                player.toggleRepeat(); // disabling repeat!

                skipBack(player.tracks.getNumberOfTracks(), player);
                player.getCurrentTrack().should.equal(lastTrack); // 1 full loop back

                skipBack(player.tracks.getNumberOfTracks() - 1, player); // from last track to first track, backwards
                player.getCurrentTrack().should.equal(firstTrack);

                player.playPrev();
                should.equal(player.getCurrentTrack(), undefined);
            });

            it('should not play any track when "previous" is requested on the first track and looping has never happened.', () => {
                player.startPlaying();
                player.playPrev();
                should.equal(player.getCurrentTrack(), undefined);
            });

            it('should not play any track when the current one gets removed and the next one would have been the current one,', () => {
                const singleTracksCollection = new TracksCollection();
                singleTracksCollection.push(new TracksCollectionItem('noop', 'url1', {}));
                player.loadTracks(singleTracksCollection);

                player.startPlaying();
                player.removeTrackByUid(player.getCurrentTrack().uid);
                should.equal(player.getCurrentTrack(), undefined);
            });
        });

        describe('with repeat track on', () => {
            beforeEach(() => {
                player.repeatTrack = true;
            });

            it('should play the same track again when "next" is requested.', () => {
                player.playTrackByNumber(3);
                player.playNext();
                player.getCurrentTrack().url.should.equal('url3');
            });

            it('should play the same track again when "previous" is requested.', () => {
                player.playTrackByNumber(3);
                player.playPrev();
                player.getCurrentTrack().url.should.equal('url3');
            });

            describe('with shuffle on', () => {
                beforeEach(() => {
                    player.shuffle = true;
                });

                it('should play the same track again when "next" is requested.', () => {
                    player.playTrackByNumber(3);
                    player.playNext();
                    player.getCurrentTrack().url.should.equal('url3');
                });

                it('should play the same track again when "previous" is requested.', () => {
                    player.playTrackByNumber(3);
                    player.playPrev();
                    player.getCurrentTrack().url.should.equal('url3');
                });
            });
        });

        describe('with shuffle on', () => {
            beforeEach(() => {
                player.shuffle = true;
            });

            it('should play a random track (not necessarily the first one) if "startPlaying" is requested.', () => {
                let firstTrackCounter = 0;
                const testTimes = 20;
                for (let i = 0; i < testTimes; i++) {
                    player.startPlaying();
                    if (player.getCurrentTrack().url === 'url1') {
                        firstTrackCounter++;
                    }
                }

                firstTrackCounter.should.be.below(testTimes);
            });

            it('should shuffle properly throughout a whole cycle, without repeating any track.', () => {
                let playedTracks = [];

                player.startPlaying();
                playedTracks.push(player.getCurrentTrack());

                for (let i = 0; i < player.tracks.getNumberOfTracks() - 1; i++) {
                    player.playNext();
                    playedTracks.should.not.contain(player.getCurrentTrack());
                    playedTracks.push(player.getCurrentTrack());
                }
            });

            it('should not play more tracks when the whole shuffle cycle has been completed.', () => {
                player.startPlaying();
                for (let i = 0; i < player.tracks.getNumberOfTracks() - 1; i++) {
                    player.playNext();
                }

                player.playNext();
                should.equal(player.getCurrentTrack(), undefined);
            });

            it('should play the previously played track (not necessarily the preceding) when "previous" is requested.', () => {
                let previouslyPlayedTracks = [];

                player.startPlaying();
                previouslyPlayedTracks.push(player.getCurrentTrack());

                player.playNext();
                previouslyPlayedTracks.push(player.getCurrentTrack());

                player.playNext();

                player.playPrev();
                player.getCurrentTrack().should.equal(previouslyPlayedTracks.pop());

                player.playPrev();
                player.getCurrentTrack().should.equal(previouslyPlayedTracks.pop());
            });

            it('should not play any track when "previous" is requested and no other tracks have been played before.', () => {
                player.playTrackByNumber(3);
                player.playPrev();
                should.equal(player.getCurrentTrack(), undefined);
            });

            it('should not play the previously played track when "previous" is requested and has been removed from the tracklist.', () => {
                let previouslyPlayedTracks = [];

                player.startPlaying();
                previouslyPlayedTracks.push(player.getCurrentTrack());

                player.playNext();
                previouslyPlayedTracks.push(player.getCurrentTrack());

                player.playNext();

                player.removeTrackByUid(previouslyPlayedTracks[1].uid);

                player.playPrev();
                player.getCurrentTrack().should.equal(previouslyPlayedTracks[0]);
            });

            it('should not track user-requested repeated tracks.', () => {
                player.playTrackByNumber(3);
                player.playNext();
                player.repeatTrack = true;
                player.playNext();
                player.repeatTrack = false;
                player.playPrev();
                player.getCurrentTrack().url.should.equal('url3');
            });

            describe('with repeat on', () => {
                beforeEach(() => {
                    player.repeat = true;
                });

                /*it('should never play the same track when a new cycle starts (except when there's only 1 track?).', () => {
                });*/
            });
        });
    });
});

function skip(number, player) {
    for (let i = 0; i < number; i++) {
        player.playNext();
    }
}

function skipBack(number, player) {
    for (let i = 0; i < number; i++) {
        player.playPrev();
    }
}

function getDefaultTracksCollection() {
    const tracksCollection = new TracksCollection();

    tracksCollection.push(new TracksCollectionItem('noop', 'url1', {}));
    tracksCollection.push(new TracksCollectionItem('noop', 'url2', {}));
    tracksCollection.push(new TracksCollectionItem('noop', 'url3', {}));
    tracksCollection.push(new TracksCollectionItem('noop', 'url4', {}));

    return tracksCollection;
}