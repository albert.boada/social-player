export default
class PlayerTransition
{
    constructor() {
        this.track;

        this.willLoop = false;
        this.willUnloop = false;
        this.willResetCycle = false;
        this.willResetPlayback = false;
        this.willTrack = true;
        this.isPlaybackTrack = false;
    }

    isStop() {
        return typeof this.track === 'undefined';
    }
}
