import SocialPlayer from './SocialPlayer.js';

export default
class SoundcloudPlayer extends SocialPlayer
{
    doLoadAndPlay(track) {
        const self = this;

        if (self.getPlayer()) {
            self.getPlayer().load(track.url, { auto_play: true, visual: true, callback: function () {
                // this callback is called even when there is an error! (AFTER it...WTF!!)
                if (!self.errored) {
                    self.onTrackLoaded();
                }
                self.errored = false;
            } });
        } else {
            SC.oEmbed(track.url, { auto_play: true }).then(function (oEmbed) {
                self.getWrapper().innerHTML = oEmbed.html.replace('<iframe', '<iframe allow="autoplay"').replace('"></iframe>', '&auto_play=true"></iframe>');
                self.setPlayer(SC.Widget(self.getWrapper().querySelector('iframe')));

                // Only called on player initial load, not when .load().
                self.getPlayer().bind('ready', function () { self.onTrackLoaded(); });

                self.getPlayer().bind('finish', function () {
                    self.finished = true;
                    self.onTrackFinished();
                });
                self.getPlayer().bind('play', function () {
                    self.started = true;
                    self.paused = false;
                    self.onStateChange(SocialPlayer.PLAY);
                });
                self.getPlayer().bind('pause', function (info) {
                    if (info.relativePosition > 0.99) {
                        // Track is finished. "finish" event will be fired too.
                        return;
                    }
                    if (self.terminating) {
                        self.terminating = false;
                        return;
                    }
                    self.paused = true;
                    self.onStateChange(SocialPlayer.PAUSE);
                });
                self.getPlayer().bind('error', function (e) {
                    self.errored = true;
                    self.onTrackLoaded();
                    self.onTrackFinished();
                });

                self.seekAttention();

                /*setTimeout(function () {
                    self.pause();
                    self.play();
                }, 300);*/
            }).catch(function (e) {
                const fakeSCPlayer = {pause: function () {}};
                self.setPlayer(fakeSCPlayer);
                self.onTrackLoaded();
                self.onTrackFinished();
                self.setPlayer(undefined);
            }).catch(function (e) {
                console.log(e);
            });
        }
    }

    doPlay() {
        this.getPlayer().play();
    }

    doPause() {
        this.getPlayer().pause();
    }

    doStop() {
        this.doPause();
    }
}
