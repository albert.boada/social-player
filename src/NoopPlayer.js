import SocialPlayer from './SocialPlayer.js';

export default
class NoopPlayer extends SocialPlayer
{
    doLoadAndPlay(track) {
        this.onTrackLoaded();
    };

    doPlay() {
    }

    doPause() {
    }

    doStop() {
    }
}
