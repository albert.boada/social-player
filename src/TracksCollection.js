export default
function TracksCollection()
{
    /** @var TracksCollectionItem[] */
    var _tracks;

    construct();

    /**
     * @param TracksCollectionItem[] tracks
     */
    this.setTracks = function (tracks) {
        resetTracks();
        _tracks = tracks;
    };

    /**
     * @return TracksCollectionItem[]
     */
    this.getTracks = function () {
        return _tracks;
    };

    /**
     * @return int
     */
    this.getNumberOfTracks = function () {
        return _tracks.length;
    }

    /**
     * @param TracksCollectionItem track
     */
    this.push = function (track) {
        _tracks.push(track);
    };

    /**
     * @param TracksCollectionItem track
     */
    this.pushFirst = function (track) {
        _tracks.unshift(track);
    };

    this.pushUnique = function (track) {
        if (this.isTrackPresent(track)) {
            return;
        }

        this.push(track);
    };

    function _getTrackIndex(track) {
        return _tracks.indexOf(track);
    }

    this.isTrackPresent = function (track) {
        return _getTrackIndex(track) !== -1;
    };

    function _getTrackByIndex(index) {
        if (typeof _tracks[index] === 'undefined') {
            throw 'Track with index #'+index+' does not exist.';
        }

        return _tracks[index];
    }

    this.getTrackByNumber = function (number) {
        try {
            return _getTrackByIndex(number-1);
        } catch (e) {
            throw 'Track with number #'+number+' does not exist.';
        }
    };

    this.removeTrackByUid = function (uid) {
        let track;
        try {
            track = this.getTrackByUid(uid);
        } catch (e) {
            // do nothing;
        }

        if (track) {
            _removeTrack(track);
        }
    };

    this.removeTracksByOriginalUid = function (uid) {
        const tracks = this.getTracksByOriginalUid(uid);
        tracks.forEach((track) => { _removeTrack(track); });
    };

    function _removeTrack(track) {
        const index = _getTrackIndex(track);
        _removeTrackByIndex(index);
    }

    function _removeTrackByIndex(index) {
        _tracks.splice(index, 1);
    }

    this.getFirstTrack = function () {
        return _getTrackByIndex(0);
    };

    this.getLastTrack = function () {
        return _getTrackByIndex(_tracks.length - 1);
    };

    /**
     * @param TracksCollection blacklist
     */
    this.getRandomTrack = function (blacklist) {
        const remainingTracks = _tracks.filter(function (track) {
            return !blacklist.isTrackPresent(track);
        });

        if (!remainingTracks.length) {
            throw 'BLACKLISTED_ALL_TRACKS';
        }

        const remainingTracksCollection = new TracksCollection();
        remainingTracksCollection.setTracks(remainingTracks);

        const randomNumber = this.getRandonNumberInRange(remainingTracksCollection.getNumberOfTracks(), 1);

        const track = remainingTracksCollection.getTrackByNumber(randomNumber);
        return track;
    }

    this.getRandonNumberInRange = function (max, min) {
        // http://stackoverflow.com/questions/1527803/generating-random-whole-numbers-in-javascript-in-a-specific-range
        return Math.floor(Math.random() * (max - min + 1)) + min;
    };

    this.getTrackByUid = function (uid) {
        const filterFn = (track) => track.uid == uid;

        const track = _tracks.find(filterFn);
        if (!track) {
            throw 'Track with uid '+uid+' does not exist.';
        }

        return track;
    };

    this.getTracksByOriginalUid = function (uid) {
        const filterFn = (track) => track.original.uid == uid;

        const tracks = _tracks.filter(filterFn);

        return tracks;
    };

    this.getTracksNextTrack = function (track) {
        function filterFn(t, index) {
            return _tracks[index-1] && _tracks[index-1] === track;
        }

        const nextTrack = _tracks.find(filterFn);
        if (!nextTrack) {
            throw 'Track with uid '+track.uid+' next track does not exist.';
        }

        return nextTrack;
    };

    this.getTracksPrevTrack = function (track) {
       function filterFn(t, index) {
            return _tracks[index+1] && _tracks[index+1] === track;
        }

        const prevTrack = _tracks.find(filterFn);
        if (!prevTrack) {
            throw 'Track with uid '+track.uid+' prev track does not exist.';
        }

        return prevTrack;
    };

    function construct() {
        reset();
    }

    function reset() {
        resetTracks();
    }

    function resetTracks() {
        _tracks = [];
    }
}
