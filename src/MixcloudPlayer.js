import SocialPlayer from './SocialPlayer.js';

export default
class MixcloudPlayer extends SocialPlayer
{
    doLoadAndPlay(track) {
        const self = this;

        if (self.getPlayer()) {
            self.getPlayer().load(track.url, true).then(function () {
                self.onTrackLoaded();
            });
        } else {
            self.getWrapper().innerHTML = `
                <iframe
                    id="mcplayer-iframe"
                    width="100%"
                    height="120"
                    src="https://www.mixcloud.com/widget/iframe/?light=1&autoplay=1&feed=`+encodeURIComponent(track.url)+`"
                    frameborder="0"
                    allow="autoplay"
                ></iframe>
            `;

            const player = Mixcloud.PlayerWidget(document.getElementById('mcplayer-iframe'));

            player.ready.then(function () {
                self.setPlayer(player);

                self.onTrackLoaded();

                self.getPlayer().events.ended.on(function () {
                    self.finished = true;
                    self.onTrackFinished();
                });

                self.getPlayer().events.pause.on(function () {
                    self.getPlayer().getPosition().then(function (position) {
                        self.getPlayer().getDuration().then(function (duration) {
                            if (self.terminating) {
                                self.terminating = false;
                                return;
                            }
                            if (position === duration) {
                                // Track is finished. "ended" event will be fired too.
                                return;
                            }
                            self.paused = true;
                            self.onStateChange(SocialPlayer.PAUSE);
                        });
                    });
                });

                self.getPlayer().events.play.on(function () {
                    self.started = true;
                    self.paused = false;
                    self.onStateChange(SocialPlayer.PLAY);
                });
            });

            self.seekAttention();
        }
    }

    doPlay() {
        this.getPlayer().play();
    }

    doPause() {
        this.getPlayer().pause();
    }

    doStop() {
        this.doPause();
    }
}
