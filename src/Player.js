import TracksCollection from './TracksCollection.js';
import TracksCollectionItem from './TracksCollectionItem.js';
import PlayerTransition from './PlayerTransition.js';

export const PUSH_MODE_APPEND  = 1;
export const PUSH_MODE_PREPEND = 2;

export default
class Player
{
    /**
     * @param SocialPlayer[] players
     * @param function onUpdate
     * @param function onUpdateStart
     */
    constructor(players, onUpdate, onUpdateStart, onStateChange) {
        /**
         * @var SocialPlayer[] players
         */
        this.players = players;

        this.onUpdate = onUpdate;
        this.onUpdateStart = onUpdateStart;
        this.onStateChange = onStateChange;

        /**
         * @var TracksCollection tracks
         *
         * Collection which stores the tracks available for the Player to play :)
         */
        this.tracks = new TracksCollection();

        /**
         * @var TracksCollection playedTracksInPlayback
         *
         * Collection used for registering all the played tracks during the
         * current Playback.
         *
         * What's exactly a Playback?
         * A Playback is one continous session of played tracks, without manual
         * alteration of its flow. A Playback can start either when the Player goes
         * from stopped to playing, or when, while playing, a track is manually
         * imposed; and lasts for as long as the Player is not stopped (e.g. the
         * Player can be stopped manually; the Player can be stopped when reaching
         * the bottom of the Playlist and looping is off; etc.), or a track is
         * manually imposed.
         *
         * This collection could potentially have repeated tracks (e.g. the same
         * list of tracks can loop many times, resulting in the same tracks being
         * added multiple times), causing unexpected results when querying it.
         * This is why this collection is not meant to directly store the original
         * TrackCollectionItems that `this.tracks` contains, but new and UNIQUE
         * TrackCollectionItems which will reference the original TrackCollectionItem
         * (i.e. `item.original`), instead.
         */
        this.playedTracksInPlayback;

        /**
         * @var TracksCollection playedTracksInCycle
         *
         * Collection used for registering all the played tracks during the
         * current Cycle, UNIQUELY.
         *
         * What's exactly a Cycle?
         * A Cycle is a time period in which not all tracks in `this.tracks` have
         * been played at least once. When all tracks in `this.tracks` have been
         * played, a new Cycle begins.
         *
         * When registering the already played tracks in the current Cycle, it makes
         * no sense to store them more than once. Being so, this collection just
         * DOES store original TrackCollectionItems, unlike `this.playedTracksInPlayback`.
         */
        this.playedTracksInCycle;

        this.repeat       = false;
        this.repeatTrack  = false;
        this.shuffle      = false;

        this.pushMode = PUSH_MODE_APPEND;

        /**
         * @var SocialPlayer currentPlayer
         */
        this.currentPlayer;

        /**
         * @var TrackCollectionItem currentPlaybackTrack
         *
         * Holds the "wrapped" TrackCollectionItem being currently played (i.e. from
         * the `this.playedTracksInPlayback` collection).
         */
        this.currentPlaybackTrack;

        /**
         * @var number loopsCount
         */
        this.loopsCount;

        this._resetPlayback();
    }

    /**
     * @param TracksCollection tracksCollection
     */
    loadTracks(tracksCollection) {
        this._resetPlayback();
        this.tracks = tracksCollection;
    }

    getCurrentTrack() {
        return this.currentPlaybackTrack ? this.currentPlaybackTrack.original : undefined;
    }

    /**
     * @param TrackCollectionItem track
     */
    addTrack(track) {
        if (this.pushMode === PUSH_MODE_PREPEND) {
           this.tracks.pushFirst(track);
        } else {
           this.tracks.push(track);
        }
    }

    removeTrackByUid(uid) {
        const isCurrentTrack = this._hasPlaybackStarted() && this.getCurrentTrack().uid === uid;
        if (isCurrentTrack) {
            this._doRemoveCurrentTrack();
        } else {
            this._doRemoveTrackByUid(uid);
        }
    }

    startPlaying() {
        const transition = this._getFirstTransition();
        this._applyTransition(transition);
    }

    playNext() {
        if (!this._hasPlaybackStarted()) {
            throw 'PLAYER_CANT_SKIP_NON_STARTED_PLAYBACK';
        }

        const transition = this._getNextTransition();
        this._applyTransition(transition);
    }

    play() {
        if (!this._hasPlaybackStarted()) {
            this.startPlaying();
            return;
        }

        this.resume();
    }

    pause() {
        if (!this._hasPlaybackStarted()) {
            throw 'PLAYER_CANT_PAUSE_NON_STARTED_PLAYBACK';
        }

        this.currentPlayer.pause();
    }

    resume() {
        if (!this._hasPlaybackStarted()) {
            throw 'PLAYER_CANT_RESUME_NON_STARTED_PLAYBACK';
        }

        this.currentPlayer.play();
    }

    skip() {
        this.playNext();
    }

    stop() {
        const transition = new PlayerTransition();

        this._applyTransition(transition);
    }

    playPrev() {
        if (!this._hasPlaybackStarted()) {
            throw 'PLAYER_CANT_BACKSKIP_NON_STARTED_PLAYBACK';
        }

        const transition = this._getPrevTransition();
        this._applyTransition(transition);
    }

    playTrackByUid(uid) {
        const transition = new PlayerTransition();
        transition.track = this.tracks.getTrackByUid(uid);
        transition.willResetPlayback = true;

        this._applyTransition(transition);
    }

    playTrackByNumber(number) {
        const uid = this.tracks.getTrackByNumber(number).uid;
        this.playTrackByUid(uid);
    }

    toggleRepeat() {
        this.repeat = !this.repeat;
        // this._resetRepeat()? <- but don't reset "this.loopsCount", ok???
        return this.repeat;
    }

    toggleRepeatTrack() {
        this.repeatTrack = !this.repeatTrack;
        return this.repeatTrack;
    }

    toggleShuffle() {
        this.shuffle = !this.shuffle;
        this._resetLoopsCount();
        this._resetPlayedTracks();
        return this.shuffle;
    }

    _resetPlayback() {
        this.currentPlaybackTrack = undefined;
        this.currentPlayer = undefined;
        this._resetLoopsCount();
        this._resetPlayedTracks();
    }

    _resetLoopsCount() {
        this.loopsCount = 0;
    }

    _resetPlayedTracks() {
        this.playedTracksInCycle = new TracksCollection();
        this.playedTracksInPlayback = new TracksCollection();
    }

    _hasPlaybackStarted() {
        return typeof this.currentPlaybackTrack !== 'undefined';
    }

    _doRemoveTrackByUid(uid) {
        this.tracks.removeTrackByUid(uid);
        this.playedTracksInPlayback.removeTracksByOriginalUid(uid);
        this.playedTracksInCycle.removeTrackByUid(uid);
    }

    _doRemoveCurrentTrack() {
        const currentTrack = this.getCurrentTrack();

        const nextTransition = this._getNextTransition();
        if (nextTransition.track && nextTransition.track.uid === currentTrack.uid) {
            nextTransition.track = undefined;
        }

        this._doRemoveTrackByUid(currentTrack.uid);
        this._applyTransition(nextTransition);
    }

    _getFirstTransition() {
        let transition;
        if (this.shuffle) {
            transition = this._getShuffleTransition();
        } else {
            transition = new PlayerTransition();
            transition.track = this.tracks.getFirstTrack();
        }
        transition.willResetPlayback = true;

        return transition;
    }

    _getNextTransition() {
        if (this.repeatTrack) {
            return this._getSameTrackTransition();
        }

        if (this.shuffle) {
            return this._getShuffleTransition();
        }

        const transition = new PlayerTransition();
        try {
            transition.track = this.tracks.getTracksNextTrack(this.getCurrentTrack());
        } catch (e) {
            if (this.repeat) {
                transition.track = this.tracks.getFirstTrack();
                transition.willLoop = true;
                transition.willResetCycle = true;
            }
        }

        return transition;
    }

    _getPrevTransition() {
        if (this.repeatTrack) {
            return this._getSameTrackTransition();
        }

        if (this.shuffle) {
            return this._getPrevShuffleTransition();
        }

        const transition = new PlayerTransition();
        try {
            transition.track = this.tracks.getTracksPrevTrack(this.getCurrentTrack());
        } catch (e) {
            if (this.loopsCount > 0) {
                transition.track = this.tracks.getLastTrack();
                transition.willUnloop = true;
            }
        }
        return transition;
    }

    _getSameTrackTransition() {
        const transition = new PlayerTransition();
        transition.track = this.currentPlaybackTrack;
        transition.isPlaybackTrack = true;
        return transition;
    }

    _getShuffleTransition() {
        const transition = new PlayerTransition();
        try {
            transition.track = this.tracks.getRandomTrack(this.playedTracksInCycle);
        } catch (e) {
            if ('BLACKLISTED_ALL_TRACKS' === e
                && this.repeat
            ) {
                transition.track = this.tracks.getRandomTrack(new TracksCollection());
                transition.willResetCycle = true;
                transition.willLoop = true;
            }
        }
        return transition;
    }

    _getPrevShuffleTransition() {
        const transition = new PlayerTransition();
        try {
            transition.track = this.playedTracksInPlayback.getTracksPrevTrack(this.currentPlaybackTrack);
            transition.isPlaybackTrack = true;
            transition.willTrack = false;
        } catch (e) { }
        return transition;
    }

    _applyTransition(transition) {
        //console.log('Transition: ', transition);
        if (!transition) {
            throw 'NO_TRANSITION';
        }

        const originalTrack = transition.isPlaybackTrack ? transition.track.original : transition.track;
        this._notifyTransitionStarted(originalTrack);

        if (this.currentPlayer) {
            this.currentPlayer.stop();
        }

        if (transition.isStop()) {
            this._resetPlayback();
            this._notifyTransitionFinished();
            return;
        } else {
            const nextPlayer = this._getPlayerForTrack(transition.track);

            nextPlayer.loadAndPlay(
                transition.track,
                function () {
                    if (transition.willResetPlayback) {
                        this._resetPlayback();
                    }

                    if (transition.willLoop) {
                        this.loopsCount++;
                    } else if (transition.willUnloop) {
                        this.loopsCount--;
                    }

                    if (transition.willResetCycle) {
                        this.playedTracksInCycle = new TracksCollection();
                    }

                    this.currentPlayer = nextPlayer;

                    let playbackTrack;
                    if (transition.isPlaybackTrack) {
                        playbackTrack = transition.track;
                    } else {
                        playbackTrack = TracksCollectionItem.makeFromOriginal(transition.track);
                    }

                    this.currentPlaybackTrack = playbackTrack;

                    if (transition.willTrack) {
                        this.playedTracksInPlayback.push(this.currentPlaybackTrack);
                        this.playedTracksInCycle.pushUnique(this.currentPlaybackTrack.original);
                    }

                    this._notifyTransitionFinished();
                }.bind(this),
                this._onTrackFinished.bind(this),
                this.onStateChange,
            );
        }
    }

    _getPlayerForTrack(track) {
        if (track) {
            return this._getPlayerForSource(track.source);
        }

        return undefined;
    }

    _getPlayerForSource(source) {
        if (typeof this.players[source] === 'undefined') {
            throw `NO_PLAYER_FOR(${source})`;
        }

        return this.players[source];
    }

    _onTrackFinished() {
        this.playNext();
    }

    _notifyTransitionStarted(futureTrack) {
        if (this.onUpdateStart) {
            this.onUpdateStart(futureTrack);
        }
    }

    _notifyTransitionFinished() {
        if (this.onUpdate) {
            this.onUpdate(this.getCurrentTrack());
        }
    }
}
