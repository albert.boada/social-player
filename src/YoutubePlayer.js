import SocialPlayer from './SocialPlayer.js';

export default
class YoutubePlayer extends SocialPlayer
{
    doLoadAndPlay(track) {
        const self = this;

        if (self.getPlayer()) {
            if (!self.isMobile()) {
                self.getPlayer().loadVideoById(track.url);
            } else {
                self.getPlayer().cueVideoById(track.url);
            }
            self.onTrackLoaded();
        } else {
            var containerId = 'youtubeembed'+Math.random().toString(16).slice(2);
            self.getWrapper().innerHTML = '<div id="'+containerId+'"></div>';
            const player = new YT.Player(containerId, {
                width: '100%',
                height: '100%',
                videoId: track.url,
                events: {
                    onReady: function (event) {
                        self.setPlayer(player);
                        self.seekAttention();
                        if (!self.isMobile()) {
                            self.play();
                        }
                        self.onTrackLoaded();
                        //event.target.playVideo();
                    },
                    onStateChange: function (event) {
                        // TODO https://stackoverflow.com/questions/41239944/youtube-api-event-distinguish-pause-from-seek-buffer
                        if (event.data === YT.PlayerState.ENDED) {
                            self.finished = true;
                            self.onTrackFinished();
                        } else if (event.data === YT.PlayerState.PAUSED) {
                            if (self.terminating) {
                                self.terminating = false;
                                return;
                            }
                            self.paused = true;
                            self.onStateChange(SocialPlayer.PAUSE);
                        } else if (event.data === YT.PlayerState.PLAYING) {
                            self.started = true;
                            self.paused = false;
                            self.onStateChange(SocialPlayer.PLAY);
                        }
                    },
                    onError: function () {
                        self.onTrackFinished();
                    }
                }
            });
        }
    };

    doPlay() {
        if (this.isMobile()) {
            return;
        }

        this.getPlayer().playVideo();
    };

    doPause() {
        if (this.isMobile()) {
            return;
        }

        this.getPlayer().pauseVideo();
    };

    doStop() {
        if (this.isMobile()) {
            return;
        }

        this.getPlayer().stopVideo();
    };
}
