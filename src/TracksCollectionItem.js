export default
class TracksCollectionItem
{
    constructor(source, url, moreInfo, original = null) {
        this.uid      = new Date().valueOf() + Math.random().toFixed(16).substring(2);
        this.source   = source;
        this.url      = url;
        this.moreInfo = moreInfo;
        this.original = original
    }

    static makeFromOriginal(original) {
        return (
            new TracksCollectionItem(original.source, original.url, original.moreInfo, original)
        );
    }
}
